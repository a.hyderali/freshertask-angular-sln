import { Component, OnInit } from "@angular/core";
import { MessageService } from "../message.service";
@Component({
  selector: "app-person1",
  templateUrl: "./person1.component.html",
  styleUrls: ["./person1.component.css"],
})
export class Person1Component implements OnInit {
  constructor(private messageService: MessageService) {}
  message: string;
  importantmessage: boolean = false;

  ngOnInit() {}

  message1(): void {
    debugger;
    if (this.message != null && this.message != "") {
      this.messageService.sendMessage(
        this.importantmessage,
        "person1",
        this.message
      );
      this.message = null;
      this.importantmessage = false;
    } else alert("Invalid Message");
  }
}
