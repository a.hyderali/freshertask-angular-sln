import { Component, OnInit } from "@angular/core";
import { MessageService } from "../message.service";

@Component({
  selector: "app-person3",
  templateUrl: "./person3.component.html",
  styleUrls: ["./person3.component.css"],
})
export class Person3Component implements OnInit {
  constructor(private messageService: MessageService) {}
  message: string;
  importantmessage: boolean = false;

  ngOnInit() {}

  message3(): void {
    debugger;
    if (this.message != null && this.message != "") {
      this.messageService.sendMessage(
        this.importantmessage,
        "person3",
        this.message
      );
      this.message = null;
      this.importantmessage = false;
    } else alert("Invalid Message");
  }
}
