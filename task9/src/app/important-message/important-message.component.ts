import { Component, OnInit } from "@angular/core";
import { MessageService } from "../message.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-important-message",
  templateUrl: "./important-message.component.html",
  styleUrls: ["./important-message.component.css"],
})
export class ImportantMessageComponent implements OnInit {
  messages: any[] = [];
  subscription: Subscription;

  constructor(private messageService: MessageService) {
    debugger;
    this.subscription = this.messageService
      .getMessage()
      .subscribe((message) => {
        debugger;
        if (message && message.important) {
          this.messages.push(message);
        }
      });
  }

  ngOnInit() {}
}
