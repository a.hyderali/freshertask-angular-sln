import { Injectable } from "@angular/core";
import { Subject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class MessageService {
  private subject = new Subject<any>();

  sendMessage(important: boolean, person: string, message: string) {
    debugger;
    this.subject.next({ important: important, person: person, text: message });
  }

  getMessage(): Observable<any> {
    debugger;
    return this.subject.asObservable();
  }

  constructor() {}
}
