import { Component, OnInit } from "@angular/core";
import { MessageService } from "../message.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-all-message",
  templateUrl: "./all-message.component.html",
  styleUrls: ["./all-message.component.css"],
})
export class AllMessageComponent implements OnInit {
  messages: any[] = [];
  subscription: Subscription;

  constructor(private messageService: MessageService) {
    debugger;
    this.subscription = this.messageService
      .getMessage()
      .subscribe((message) => {
        if (message) {
          this.messages.push(message);
        } else {
          this.messages = [];
        }
      });
  }

  ngOnInit() {}
}
