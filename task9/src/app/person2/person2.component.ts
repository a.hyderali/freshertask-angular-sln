import { Component, OnInit } from "@angular/core";
import { MessageService } from "../message.service";

@Component({
  selector: "app-person2",
  templateUrl: "./person2.component.html",
  styleUrls: ["./person2.component.css"],
})
export class Person2Component implements OnInit {
  constructor(private messageService: MessageService) {}
  message: string;
  importantmessage: boolean = false;

  ngOnInit() {}

  message2(): void {
    debugger;
    if (this.message != null && this.message != "") {
      this.messageService.sendMessage(
        this.importantmessage,
        "person2",
        this.message
      );
      this.message = null;
      this.importantmessage = false;
    } else alert("Invalid Message");
  }
}
