export class ticketDataVM{
    EnquiryId : string;
    CreatedDatetime:string;
    CreatedBy:string;
    ContractNumber:string;
    LabourNumber:string;
    LabourName:string;
    StatusName:string;
    TicketTypeName:string;
    PriorityName:string;
    Accuracy:string;
    Subject:string;

}