import { Component, OnInit, Input, EventEmitter,Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TicketService } from '../ticket.service'
import{ticketVM} from '../interface/ticketVM'
@Component({
  selector: 'app-ticket-modal',
  templateUrl: './ticket-modal.component.html',
  styleUrls: ['./ticket-modal.component.css']
})
export class TicketModalComponent implements OnInit {

  @Input() mypopup: boolean = false;
  @Output() closeModal = new EventEmitter<any>();
  operation : string = "A";

  constructor(private fb: FormBuilder,private ticketservice :TicketService) { }
  submited = false;
  ddlLeaveType:[];
  ContractForm = this.fb.group({  
    ContractNumber: ['', Validators.required],
    LabourID:[''],
    TicketType:['', Validators.required],
    Department:['', Validators.required],
    AssignedTo:[''],
    Priority:[''],
    Category:['', Validators.required],
    SubCategory:['', Validators.required],
    TicketSubject:['', Validators.required],
    Description:['', Validators.required],
    UploadFile:['']

  });
 
  ngOnInit(): void {
this.getContract();
this.getTickettype();
this.getPriority();
  }
  
  submit(){
    debugger;
    this.submited = true;
    var isValid = true;
    const ticket = this.ContractForm.value as ticketVM;

    if (ticket.ContractNumber == "") {
      isValid = false
    }
    if (ticket.Category == "") {
      isValid = false
    }
    if (ticket.SubCategory == "") {
      isValid = false
    }
    if (ticket.TicketSubject == "") {
      isValid = false
    }
    if (ticket.TicketType == "") {
      isValid = false
    }
    if (ticket.Description == "") {
      isValid = false
    }
    if (ticket.Department == "") {
      isValid = false
    }
    if (isValid) {
      if (this.operation == "A") {
        this.add(ticket);
      } else {
        this.update(ticket);
      }
      this.closePopup();
    } else {
      alert("please fill out the fields")
    }
  }
  closePopup(){
    debugger
    this.mypopup = false;
    this.closeModal.emit();
  }
  uploadFile: File = null;
  fileInputget(files: FileList){
    this.uploadFile = files.item(0);
  }
add(ticket:ticketVM){
  debugger
  const formData = new FormData();
  formData.append(this.uploadFile.name, this.uploadFile);

	var params = JSON.stringify({
		cusnam: "",
		custid: this.ticketservice.SerachCustomerID,
		emil: "",
		descp: ticket.Description,
		contno: "",
		Page: "",
		PageSize: "",
		Priority: ticket.Priority,
		Group: ticket.Category,
		SubGroup: ticket.SubCategory,
		Subject: ticket.TicketSubject,
		AssignedTo: ticket.AssignedTo,
		TicketType: ticket.TicketType,
		TicketAssignGroup: ticket.Department,
		ContractNumber: ticket.ContractNumber,
		LabourNumber: ticket.LabourID,
		TicketChannel: 2,
		UserId: "cc.user",
		accuracy: "",
  });
  this.ticketservice.createTicket(params,formData).subscribe(result=>
    {
      alert(result);
    },
    (error:any)=>{
      debugger
      alert(error);
    }
    )
}
update(ticket:ticketVM){

}

contractList : any [];
getContract() {
  debugger
  this.ticketservice.getContract().subscribe(result=>
    {
      this.contractList = result;
    },
    (error:any)=>{
      debugger
      alert(error);
    }
    )
   
	
}
ticketTypeList:any[];
getTickettype() {
  debugger
this.ticketservice.getTickettype().subscribe(result=>
  {
    this.ticketTypeList = result
  },
  (error:any)=>{
    debugger
    alert(error);
  }
  )
}
 priorityList : any[];
 getPriority() {
  debugger
  this.ticketservice.getPriority().subscribe(result=>
    {
      this.priorityList = result
    },
    (error:any)=>{
      debugger
      alert(error);
    }
    )
}
assignToList:any[];
getAssigntoGroup(assignGroupId:string) {
  debugger
  this.ticketservice.getAssigntoGroup(assignGroupId).subscribe(result=>
    {
      this.assignToList = result;
      this.getCategory(assignGroupId);
    },
    (error:any)=>{
      debugger
      alert(error);
    }
    )
}
categoryList:any[];
getCategory(assignGroupId:string) {
  debugger
  this.ticketservice.getCategory(assignGroupId).subscribe(result=>
    {
      this.categoryList = result;
    },
    (error:any)=>{
      debugger
      alert(error);
    })
}

subcategoryList:any[];
getSubCategory(categoryId:string) {
  debugger
  this.ticketservice.getSubCategory(categoryId).subscribe(result=>
    {
      this.subcategoryList = result;
    },
    (error:any)=>{
      debugger
      alert(error);
    })
}


departmentList:any[];
getDepartment(tickettypeId:string) {
  debugger
  this.ticketservice.getDepartment(tickettypeId).subscribe(result=>
    {
      this.departmentList = result;
    },
    (error:any)=>{
      debugger
      alert(error);
    })
}


labourList:any[];
getLabour(contractId:string) {
  debugger
  this.ticketservice.getLabour(contractId).subscribe(result=>
    {
      this.labourList = result;
    },
    (error:any)=>{
      debugger
      alert(error);
    })
}


}
