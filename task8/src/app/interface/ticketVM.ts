export interface ticketVM
{
    ContractNumber:string;
    LabourID:string;
    TicketType:string;
    Department:string;
    AssignedTo:string;
    Priority:string;
    Category:string;
    SubCategory:string;
    TicketSubject:string;
    Description:string;
    UploadFile:string;
}