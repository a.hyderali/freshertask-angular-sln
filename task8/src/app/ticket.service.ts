import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {ticketDataVM} from './TicketDataVM'
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private http: HttpClient) { }
  APiUrl: string = "https://erp.arco.sa:65/" ;
  SerachCustomerID : string = "CIN0000150";
  url:string = "";

  getTicket(filterstatus : string):Observable<ticketDataVM[]> {
    debugger
    if (filterstatus && filterstatus != "-2") {
     this.url = "/api/GetMyTicket?CustomerId=" + this.SerachCustomerID + "&status=" + filterstatus;
  }
  else {
    this.url = "/api/GetMyTicket?CustomerId=" + this.SerachCustomerID;
  }
    return this.http.get(`${this.APiUrl}` + this.url).pipe(map((x:any) =>x.Data as ticketDataVM[]))
  }
  
  getContract():Observable<any[]> {
    debugger
    this.url = "/api/GetTicketCustomerDetails?CustomerId=" +this.SerachCustomerID;
    return this.http.get(`${this.APiUrl}` + this.url).pipe(map((x:any) =>x.Result2))
  }
  getTickettype():Observable<any[]> {
    debugger
    this.url = "/api/TickettypeList";
    return this.http.get(`${this.APiUrl}` + this.url).pipe(map((x:any) =>x.Data))
  }
  getPriority():Observable<any[]> {
    debugger
    this.url = "/api/PriorityList";
    return this.http.get(`${this.APiUrl}` + this.url).pipe(map((x:any) =>x.Data))
  }
  getAssigntoGroup(assignGroupId:string):Observable<any[]> {
    debugger
    this.url = "api/GetAssignToByGroup?AssignGroupId=" + assignGroupId;
    return this.http.get(`${this.APiUrl}` + this.url).pipe(map((x:any) =>x.Data))
  }
  getCategory(assignGroupId:string):Observable<any[]> {
    debugger
    this.url =  "api/GetTicketGroupByDepatmentId?TicketAssignGroupId=" + assignGroupId;
    return this.http.get(`${this.APiUrl}` + this.url).pipe(map((x:any) =>x))
  }
  getSubCategory(categoryId:string):Observable<any[]> {
    debugger
    this.url =  "/api/SubGroupByGroup?id=" + categoryId;
    return this.http.get(`${this.APiUrl}` + this.url).pipe(map((x:any) =>x))
  }
  getDepartment(tickettypeId:string):Observable<any[]> {
    debugger
    this.url =  "/api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID=" + tickettypeId;
    return this.http.get(`${this.APiUrl}` + this.url).pipe(map((x:any) =>x))
  }
  getLabour(contractId:string):Observable<any[]> {
    debugger
    this.url =  "/api/GetTicketIndContractAllEmployee?CustomerId=" + this.SerachCustomerID + "&ContractId=" + contractId; 
    return this.http.get(`${this.APiUrl}` + this.url).pipe(map((x:any) =>x))
  }
  
  createTicket(params:any, formdata) {
    debugger;
    this.url = '/api/CreateTicketNew?UpdateTicketFields=' + params;
    const content_ = formdata;

    let options_: any = {
      body: content_,
      observe: "response",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };
    return this.http.post(`${this.APiUrl}` + this.url, { observe: 'response' })
  }

}
