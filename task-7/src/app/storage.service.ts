import { Injectable } from '@angular/core';
import { studentModel } from './studentModel';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }
  studentModelVM :  studentModel;
  getStudent():[]
  {
    debugger
    let student = JSON.parse(localStorage.getItem("Studentdata"));
    if(student == null)
    {
      return  [];
    }
    else
    {
      return student;
    }
  }

  addStudent(newstudent: any)
  {
    debugger
    let student = JSON.parse(localStorage.getItem("Studentdata"));
    if(student == null)
    {
      student = [];
    }
    student.push(newstudent);
    localStorage.setItem("Studentdata", JSON.stringify(student));
  }
  updateStudent(newstudent : any, selected_index : string)
  {
    debugger
    let student = JSON.parse(localStorage.getItem("Studentdata"));
    student[selected_index] = newstudent;
    localStorage.setItem("Studentdata", JSON.stringify(student));
  }
  getbyindexid(selected_index : string): studentModel
  {
    debugger
    let student = JSON.parse(localStorage.getItem("Studentdata"));
    return this.studentModelVM = student[selected_index];
  }
  delete(selected_index : string)
  {
    debugger
    let student = JSON.parse(localStorage.getItem("Studentdata"));
    student.splice(selected_index, 1);
	  localStorage.setItem("Studentdata", JSON.stringify(student));
  }
  
}
