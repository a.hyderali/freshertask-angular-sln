import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {studentModel} from '../studentModel';
import {StorageService} from '../storage.service';
import { of } from 'rxjs';
@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css']
})
export class CrudComponent implements OnInit {
  constructor(private fb: FormBuilder,private storageservice : StorageService) {
  }

  operation: string = "A";
  validEmail : boolean = true;
  selected_index: any = -1;
  mypopup: boolean = false;
  submited : boolean = false;
  Studentlist : [] ;
  studentForm = this.fb.group({
    StudentName: ['', Validators.required],
    FatherName: ['', Validators.required],
    DateOfBirth: ['', Validators.required],
    Department: ['', Validators.required],
    Address: ['', Validators.required],
    Age: ['', Validators.required],
    EmailId: ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
    PassWord: ['', [Validators.required,Validators.minLength(5)]],
    ConfirmPassWord: ['',  [Validators.required,Validators.minLength(5)]],
    Gender: ['', Validators.required],
  });
  showPopup() {
    debugger
    this.mypopup = true;
    this.submited = false;
  }
  closePopup() {
    debugger
    this.mypopup = false;
    this.submited = false; 
  }
  // password(formGroup: FormGroup) {
  //   debugger
  //   const { value: PassWord } = formGroup.get('PassWord');
  //   const { value: ConfirmPassWord } = formGroup.get('ConfirmPassWord');
  //   return PassWord == ConfirmPassWord ? of(null) : of({ passwordNotMatch: true });
  // }

  ConfirmedValidator(controlName: string, matchingControlName: string){
    debugger
    // return (formGroup: FormGroup) => {
        const control = this.studentForm.get(controlName);
        debugger
        const matchingControl = this.studentForm.get(matchingControlName);
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ confirmedValidator: true });
        } else {
            matchingControl.setErrors(null);
        }
    // }
}
  submit() {
    debugger
    this.submited = true;
    var isValid = true;
    const studentdata = this.studentForm.value as studentModel
    if (studentdata.StudentName == "") {
      isValid = false
    }
    if (studentdata.FatherName == "") {
      isValid = false
    }
    if (studentdata.DateOfBirth == "") {
      isValid = false
    }
    if (studentdata.Department< 0) {
      isValid = false
    }
    if (studentdata.Address == "") {
      isValid = false
    } 
    if (studentdata.Age < 0) {
      isValid = false
    } 
    if (studentdata.EmailId == "") {
      isValid = false
    } 
   
    if (this.operation == "A") {
      if (studentdata.PassWord == "") {
        isValid = false
      }
      if (studentdata.ConfirmPassWord == ""){
        isValid = false
      } 
    }
    if (isValid) {
      if (this.operation == "A") {
        this.add(studentdata);
      } else {
       this.update(studentdata);
      }
     this.closePopup();
    } else {
      alert("please fill out the fields")
    }
  }

add(studentdata : any) {
    debugger
    this.storageservice.addStudent(studentdata);
    alert("The data was saved.");
    this.ngOnInit();
}
  
update(studentdata : any) {
  debugger
 this.storageservice.updateStudent(studentdata,this.selected_index);
	alert("record updated.")
	this.operation = "A";
	this.ngOnInit();
}
editClick(params) {
	debugger
	this.operation = "E";
  this.selected_index = params;
  let value = this.storageservice.getbyindexid(this.selected_index)
   this.studentForm.setValue({
    StudentName: value.StudentName,
    FatherName: value.FatherName,
    DateOfBirth: value.DateOfBirth,
    Department: value.Department,
    Address: value.Address,
    Age: value.Age,
    EmailId: value.EmailId,
    Gender: value.Gender,
    PassWord: value.PassWord,
    ConfirmPassWord: value.ConfirmPassWord,
    })
  this.showPopup();
}
deleteClick(params) {
	debugger
  this.selected_index = params;
  this.storageservice.delete(this.selected_index);
  alert("record deleted.");
  this.ngOnInit();
}
  ngOnInit(): void {
    debugger
    this.Studentlist=  this.storageservice.getStudent();
    this.studentForm.get('ConfirmPassWord').valueChanges
    .subscribe(x=> this.ConfirmedValidator("PassWord","ConfirmPassWord"));
  }

  emailValidate(emailField){
    debugger
    let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(emailField) == false) 
    {
      return this.validEmail = false;
    }
    return this.validEmail = true;
  }
}
